// index.js

/**
 * Shape class
 * @param x
 * @param y
 * @constructor
 */
function Shape(x, y) {
  this.x = x;
  this.y = y;
  this.isSplit = false;
  this.id = new Date().getTime();
}

/**
 * Create rectangle function
 * @param x
 * @param y
 * @param width
 * @param height
 * @returns {Shape}
 * @constructor
 */
function Rectangle(x, y, width, height) {
  const shape = new Shape(x, y);
  shape.width = width;
  shape.height = height;

  shape.getPosition = function () {
    return {
      x: this.x,
      y: this.y,
      width: this.width,
      height: this.height
    }
  }

  shape.draw = function (element) {
    const ctx = element.getContext('2d');
    if (this.isSplit) {
      const w = this.width / 2;
      ctx.beginPath();
      ctx.fillStyle = '#005082';
      ctx.fillRect(this.x, this.y, w - 2, this.height);
      ctx.fill();

      ctx.beginPath();
      ctx.fillStyle = '#005082';
      ctx.fillRect(this.x + w, this.y, w, this.height);
      ctx.fill();
    } else {
      ctx.beginPath();
      ctx.fillStyle = '#005082';
      ctx.fillRect(this.x, this.y, this.width, this.height);
      ctx.fill();
    }
  }

  return shape;
}

/**
 * Create circle function
 * @param x
 * @param y
 * @param radius
 * @returns {Shape}
 * @constructor
 */
function Circle(x, y, radius) {
  const shape = new Shape(x, y);
  shape.width = 50;
  shape.height = 50;

  shape.getPosition = function () {
    return {
      x: this.x - 50,
      y: this.y - 50,
      width: this.width + 50,
      height: this.height + 50
    }
  }

  shape.draw = function (element) {
    const radius = 50;
    const ctx = element.getContext('2d');
    ctx.beginPath();
    ctx.fillStyle = "#00bdaa";
    if (this.isSplit) {
      this.width = this.width + 5;
      ctx.arc(this.x, this.y, radius, 90 * (Math.PI / 180), 270 * (Math.PI / 180), false);
      ctx.fill();

      ctx.beginPath();
      ctx.fillStyle = "#00bdaa";
      ctx.arc(this.x + 5, this.y, radius, 90 * (Math.PI / 180), 270 * (Math.PI / 180), true);
      ctx.fill();
    } else {
      ctx.arc(this.x, this.y, radius, 0, 2 * Math.PI, false);
      ctx.fill();
    }
  }

  return shape;
}

/**
 * Create triangle function
 * @param x
 * @param y
 * @returns {Shape}
 * @constructor
 */
function Triangle(x, y) {
  const shape = new Shape(x, y);
  shape.width = 100;
  shape.height = 100;

  shape.getPosition = function () {
    return {
      x: this.x - 50,
      y: this.y,
      width: 100,
      height: 100
    }
  }

  shape.draw = function (element) {
    const ctx = element.getContext('2d');
    ctx.beginPath();
    ctx.fillStyle = "#ffa41b";
    if (this.isSplit) {
      ctx.beginPath();
      ctx.moveTo(this.x - 2, this.y);
      ctx.lineTo(this.x - 50, this.y + 100); // draw straight down
      ctx.lineTo(this.x - 2, this.y + 100); // draw straight down
      ctx.closePath();
      ctx.fill();

      ctx.beginPath();
      ctx.moveTo(this.x + 2, this.y);
      ctx.lineTo(this.x + 2, this.y + 100); // draw straight down
      ctx.lineTo(this.x + 50, this.y + 100); // draw straight down
      ctx.closePath();
      ctx.fill();
    } else {
      ctx.beginPath();
      ctx.moveTo(this.x, this.y);
      ctx.lineTo(this.x - 50, this.y + 100); // draw straight down
      ctx.lineTo(this.x + 50, this.y + 100); // draw straight down
      ctx.closePath();
      ctx.fill();
    }
  }
  return shape;
}

/**
 * Canvas Class
 * @param target
 * @constructor
 */
function Canvas(target) {
  this.target = target
  this.shapes = [];
}

// Clear all the shapes in canvas
Canvas.prototype.clearDrawings = function () {
  const context = this.target.getContext('2d');
  context.clearRect(0, 0, this.target.width, this.target.height);
};

// draw all the objects
Canvas.prototype.draw = function () {
  this.shapes.forEach((shape) => shape.draw(this.target));
};

// add new shape
Canvas.prototype.addShape = function (shape) {
  this.shapes.push(shape);
}

// identify the selected shape
Canvas.prototype.identityShape = function (x, y) {
  let selectedShape = null;
  for (let i = 0; i < this.shapes.length; i++) {
    const shape = this.shapes[i];
    const position = shape.getPosition();
    const shapeX = position.x + position.width;
    const shapeY = position.y + position.height;
    if ((position.x <= x && position.y <= y) && (shapeX >= x && shapeY >= y)) {
      selectedShape = shape;
      break;
    }
  }

  console.log('identified shape:', selectedShape);
  return selectedShape;
}

// remove shape from canvas
Canvas.prototype.removeShape = function (shape) {
  this.shapes = this.shapes.filter((s) => s.id !== shape.id);
}

/**
 * Get canvas mouse position
 * @param clientX
 * @param clientY
 * @returns {{x: number, y: number}}
 */
Canvas.prototype.getCanvasMousePosition = function (clientX, clientY) {
  const rect = this.target.getBoundingClientRect();
  return {
    x: clientX - rect.left,
    y: clientY - rect.top
  };
}

// Ready function
const readyFunction = () => {
  console.log('initializing dom');

  // get all the necessary dom elements
  const canvasElement = document.getElementById('canvasObject');
  const btnRect = document.getElementById('btnRect');
  const btnCircle = document.getElementById('btnCircle');
  const btnTriangle = document.getElementById('btnTriangle');
  const btnErase = document.getElementById('btnErase');
  const btnCut = document.getElementById('btnCut');
  const btnGlue = document.getElementById('btnGlue');

  // create the canvas instance
  const canvas = new Canvas(canvasElement);

  // DnD logic
  function onDragStart(event) {
    event
      .dataTransfer
      .setData('text/plain', event.target.id);

    event
      .currentTarget
      .style
      .backgroundColor = '#ff7c7c';
  }

  function onDragOver(event) {
    event.preventDefault();
  }

  function onDrop(event) {
    const id = event
      .dataTransfer
      .getData('text');

    const draggableElement = document.getElementById(id);
    const type = draggableElement.getAttribute('data-btn-type');

    draggableElement.style.backgroundColor = 'white';
    if (canvasElement) {
      const {x, y} = canvas.getCanvasMousePosition(event.clientX, event.clientY);
      switch (type) {
        case 'rectangle': {
          canvas.addShape(new Rectangle(x, y, 100, 100));
          canvas.draw();
          break;
        }
        case 'circle': {
          canvas.addShape(new Circle(x, y, 50));
          canvas.draw();
          break;
        }
        case 'triangle': {
          canvas.addShape(new Triangle(x, y));
          canvas.draw();
          break;
        }
        case 'erase': {
          const shape = canvas.identityShape(x, y);
          if (shape) {
            canvas.removeShape(shape);
            canvas.clearDrawings();
            canvas.draw();
          }
          break;
        }
        case 'cut': {
          const shape = canvas.identityShape(x, y);
          if (shape) {
            canvas.removeShape(shape);
            shape.isSplit = true;
            canvas.addShape(shape);
            canvas.clearDrawings();
            canvas.draw();
          }
          break;
        }
        case 'join': {
          const shape = canvas.identityShape(x, y);
          if (shape) {
            canvas.removeShape(shape);
            shape.isSplit = false;
            canvas.addShape(shape);
            canvas.clearDrawings();
            canvas.draw();
          }
          break;
        }
        default:
          break;
      }
    } else {
      console.error('canvas element not found');
    }

    event
      .dataTransfer
      .clearData();
  }

  // listen for events
  btnRect.addEventListener('dragstart', onDragStart);
  btnCircle.addEventListener('dragstart', onDragStart);
  btnTriangle.addEventListener('dragstart', onDragStart);
  btnErase.addEventListener('dragstart', onDragStart);
  btnCut.addEventListener('dragstart', onDragStart);
  btnGlue.addEventListener('dragstart', onDragStart);
  canvasElement.addEventListener('dragover', onDragOver);
  canvasElement.addEventListener('drop', onDrop);
}


// On document ready
document.addEventListener('DOMContentLoaded', readyFunction);